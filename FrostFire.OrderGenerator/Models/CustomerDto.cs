﻿using System;
using System.Collections.Generic;
using System.Text;
using FrostFire.Models;

namespace FrostFire.OrderGenerator.Models
{
    public class CustomerDto : Customer
    {
        public override string ToString() => $"{FamilyName}, {GivenName}";
    }
}
