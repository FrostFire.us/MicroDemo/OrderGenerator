﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace FrostFire.OrderGenerator.Models
{
    public class OrderDto : FrostFire.Models.Order
    {
        private string _orderID;

        public new string OrderID => GetOrderID();
        public new CustomerDto Customer { get; set; }
        [JsonIgnore]
        public double Total => Items.Sum(items => items.Price);

        private string GetOrderID()
        {
            if (string.IsNullOrWhiteSpace(_orderID))
                _orderID = Guid.NewGuid().ToString();
            return _orderID;
        }
    }
}
