﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrostFire.OrderGenerator.Interfaces
{
    public interface IProduct
    {
        string ProductLine { get; set; }
        string ProductType { get; set; }
        string ProductName { get; set; }
        double Price { get; set; }
    }
}
