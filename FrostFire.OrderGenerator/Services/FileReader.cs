﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrostFire.OrderGenerator.Interfaces;
using FrostFire.OrderGenerator.Models;

namespace FrostFire.OrderGenerator.Services
{
    public class FileReader
    {
        public IEnumerable<IProduct> GetProducts(string FilePath)
        {
            FilePath = Path.GetFullPath(FilePath);

            if (!FileExists(FilePath))
                throw new FileNotFoundException(FilePath);

            List<Product> products = new List<Product>();
            using (TextReader tr = GetReader(FilePath))
                while (tr.Peek() > 0)
                {
                    string[] facts = tr.ReadLine()?.Split(',');
                    if (facts is null || facts.Length == 0)
                        continue;
                    products.Add(new Product
                    {
                        ProductLine = facts[3],
                        ProductType = facts[4],
                        ProductName = facts[5],
                        Price = double.Parse(facts[8]) / double.Parse(facts[9])
                    });
                }

            return products;
        }

        public IEnumerable<string> GetCountries(string FilePath)
        {
            FilePath = Path.GetFullPath(FilePath);
            if (!FileExists(FilePath))
                throw new FileNotFoundException();

            List<string> countries = new List<string>();
            using (TextReader tr = GetReader(FilePath))
                while (tr.Peek() > 0)
                {
                    string[] facts = tr.ReadLine()?.Split(',');
                    if(facts is null || facts.Length==0)
                        continue;
                    countries.Add(facts[0]);
                }

            return countries.Distinct().ToList();
        }

        public IEnumerable<string> GetOrderTypes(string FilePath)
        {
            FilePath = Path.GetFullPath(FilePath);
            if (!FileExists(FilePath))
                throw new FileNotFoundException();

            List<string> orderTypes = new List<string>();
            using (TextReader tr = GetReader(FilePath))
                while (tr.Peek() > 0)
                {
                    string[] facts = tr.ReadLine()?.Split(',');
                    if (facts is null || facts.Length == 0)
                        continue;
                    orderTypes.Add(facts[1]);
                }

            return orderTypes.Distinct().ToList();
        }

        public IEnumerable<string> GetRetailTypes(string FilePath)
        {
            FilePath = Path.GetFullPath(FilePath);
            if (!FileExists(FilePath))
                throw new FileNotFoundException();

            List<string> retailTypes = new List<string>();
            using (TextReader tr = GetReader(FilePath))
                while (tr.Peek() > 0)
                {
                    string[] facts = tr.ReadLine()?.Split(',');
                    if (facts is null || facts.Length == 0)
                        continue;
                    retailTypes.Add(facts[2]);
                }

            return retailTypes.Distinct().ToList();
        }

        public IEnumerable<string> GetCustomerNames(string FilePath)
        {
            FilePath = Path.GetFullPath(FilePath);
            if(!FileExists(FilePath))
                throw new FileNotFoundException();
            
            using (TextReader tr = GetReader(FilePath))
                 return tr.ReadToEnd().Split(new []{"\r","\n"}, StringSplitOptions.RemoveEmptyEntries).Distinct();
        }
        

        private static TextReader GetReader(string Path)
        {
            return File.OpenText(Path);
        }

        private static bool FileExists(string Path)
        {
            return File.Exists(Path);
        }

        private class Product : IProduct
        {
            public string ProductLine { get; set; }
            public string ProductType { get; set; }
            public string ProductName { get; set; }
            public double Price { get; set; }
        }
    }
}
