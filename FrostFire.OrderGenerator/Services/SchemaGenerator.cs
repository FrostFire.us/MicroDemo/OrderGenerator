﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Newtonsoft.Json.Schema.Generation;

namespace FrostFire.OrderGenerator.Services
{
    public class SchemaGenerator<T> where T : class
    {
        public string Generate()
        {
            JSchemaGenerator generator = new JSchemaGenerator();
            return generator.Generate(typeof(T)).ToString();
        }
    }
}
