﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using FrostFire.Models;
using FrostFire.OrderGenerator.Helpers;
using FrostFire.OrderGenerator.Interfaces;
using FrostFire.OrderGenerator.Services;
using FrostFire.OrderGenerator.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FrostFire.OrderGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generate Schema
            //string schema = new SchemaGenerator<Order>().Generate();
            //File.WriteAllText(Directory.GetCurrentDirectory() + "\\OrdersSchema.json", schema);

            //Load the settings file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);

            //Launch
            new Worker(builder.Build()).Run();
        }


    }

    public class Worker : IDisposable
    {
        private readonly IConfigurationRoot _configuration;
        private HttpClient _client;

        public Worker(IConfigurationRoot Configuration)
        {
            _configuration = Configuration;
        }

        public void Run()
        {
            Inventory.Products = LoadProducts().ToList();
            Inventory.Countries = LoadCountries().ToList();
            Inventory.OrderTypes = LoadOrderTypes().ToList();
            Inventory.RetailerType = LoadRetailTypes().ToList();
            Customers.GivenNames = LoadGivenNames().ToList();
            Customers.FamilyNames = LoadFamilyNames().ToList();

            Generator();
        }

        private void Generator()
        {
            Console.WriteLine("Generating and sending orders...press 'ESC' to stop.");
            Console.WriteLine();

            Random rnd = new Random();

            //Ignore SSL errors
            HttpClientHandler handler = new HttpClientHandler { ServerCertificateCustomValidationCallback = (Message, Certificate2, Arg3, Arg4) => true };

            _client = new HttpClient(handler) { BaseAddress = new Uri(_configuration["ReceiverServer"]) };

            do
            {
                while (!Console.KeyAvailable)
                {
                    OrderDto order = new OrderDto
                    {
                        OrderDateTime = DateTime.Now,
                        Customer = GetCustomer(),
                        Items = GetOrderItems(rnd, int.Parse(_configuration["MaxOrderItems"]))
                    };

                    _client.PostAsync(_configuration["ReceiverServerAPISubmit"], new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json")).Wait();

                    Console.WriteLine($"Sent Order: {order.OrderID,-40} {order.Customer,-30} {order.Items.Count(),-5} {order.Total:C}");
                    Thread.Sleep(int.Parse(_configuration["SendInterval"]));
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);


        }

        private static CustomerDto GetCustomer()
        {
            return new CustomerDto
            {
                GivenName = Customers.GetRandom(Customers.GivenNames),
                FamilyName = Customers.GetRandom(Customers.FamilyNames)
            };
        }

        private static IEnumerable<OrderItem> GetOrderItems(Random RNG = null, int MaxItems = 1)
        {
            if (RNG is null)
                RNG = new Random();
            List<OrderItem> items = new List<OrderItem>();
            for (int i = 0; i < RNG.Next(1, MaxItems); i++)
            {
                IProduct product = Inventory.GetRandom(Inventory.Products);
                items.Add(new OrderItem
                {
                    RetailerCountry = Inventory.GetRandom(Inventory.Countries),
                    RetailerType = Inventory.GetRandom(Inventory.RetailerType),
                    OrderMethod = Inventory.GetRandom(Inventory.OrderTypes),
                    Quantity = RNG.Next(1, 5),
                    ProductLine = product.ProductLine,
                    ProductType = product.ProductType,
                    ProductName = product.ProductName,
                    Price = product.Price
                });
            }
            return items;
        }

        private IEnumerable<IProduct> LoadProducts()
        {
            return new FileReader().GetProducts(_configuration["ProductsFile"]);
        }
        private IEnumerable<string> LoadCountries()
        {
            return new FileReader().GetCountries(_configuration["ProductsFile"]);
        }
        private IEnumerable<string> LoadOrderTypes()
        {
            return new FileReader().GetOrderTypes(_configuration["ProductsFile"]);
        }
        private IEnumerable<string> LoadRetailTypes()
        {
            return new FileReader().GetRetailTypes(_configuration["ProductsFile"]);
        }
        private IEnumerable<string> LoadGivenNames()
        {
            return new FileReader().GetCustomerNames(_configuration["FirstNames"]);
        }
        private IEnumerable<string> LoadFamilyNames()
        {
            return new FileReader().GetCustomerNames(_configuration["LastNames"]);
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}
