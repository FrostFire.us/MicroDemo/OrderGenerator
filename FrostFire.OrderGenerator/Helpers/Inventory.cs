﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrostFire.OrderGenerator.Interfaces;
using FrostFire.OrderGenerator.Models;

namespace FrostFire.OrderGenerator.Helpers
{
    public static class Inventory
    {
        private static readonly Random _rnd = new Random();
        public static List<IProduct> Products { get; set; }
        public static List<string> Countries { get; set; }
        public static List<string> OrderTypes { get; set; }
        public static List<string> RetailerType { get; set; }


        public static T GetRandom<T>(List<T> Source)
        {
            if (Source is null || !Source.Any())
                return default(T);

            return Source[_rnd.Next(0, Source.Count - 1)];
        }
    }
}
