﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrostFire.OrderGenerator.Helpers
{
    public static class Customers
    {
        private static readonly Random _rnd = new Random();
        public static List<string> GivenNames { get; set; }
        public static List<string> FamilyNames { get; set; }

        public static string GetRandom(List<string> Source)
        {
            if (Source is null || !Source.Any())
                return null;

            return Source[_rnd.Next(0, Source.Count - 1)];
        }

        
    }
}
